// Setup Express dependency
const express = require("express");
// Create a Router Instance
const router = express.Router();
// import TaskController.js
const taskController = require("../controllers/taskController")

// Routes

// Route to get all tasks
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for deleting a task
// localhost:3001/tasks/219a71984194ba
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route for updating a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;

// 1. Create a route for getting a specific task.
// 3. Return the result back to the client/Postman(Routes).
router.get("/:id", (req, res) => {
	taskController.findTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


// 5. Create a route for changing the status of a task to "complete".
// 7. Return the result back to the client/Postman(Routes).
router.put("/:id/complete", (req, res) => {
	taskController.statusTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


// ACTIVITY STEPS IN POSTMAN:
// 4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
// 8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
